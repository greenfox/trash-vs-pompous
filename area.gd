extends Area

export var health:int = 15


func _on_area_body_entered(body):
	body.damage(-health)
	queue_free()
	pass # Replace with function body.
