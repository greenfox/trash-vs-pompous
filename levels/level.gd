class_name Level
extends Spatial

export var spawner:NodePath="Spawner"

func getSpawner()->Spatial:
	return (get_node(spawner) as Spatial)
