#!/bin/bash

targetPath="./elements/tools/gitLog.gd"

file()
{
cat << EOF
extends Node

func _ready():
	pause_mode = Node.PAUSE_MODE_PROCESS
	
func _input(event):
	if event is InputEventKey and event.pressed and event.scancode == KEY_L and event.control and event.shift:
			print_debug(git_log)
	
var git_log: = """
$(git log)
"""
EOF
}


file > $targetPath
