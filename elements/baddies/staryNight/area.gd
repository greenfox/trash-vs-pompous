extends Area

signal fightStarted
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_area_body_entered(body):
	get_parent().startFight()
	body.flatten(Globals.FSTATE.WIDE)
	emit_signal("fightStarted")

func _on_area_body_exited(body):
	body.flatten(Globals.FSTATE.THICK)
