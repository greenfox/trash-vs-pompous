extends AudioStreamPlayer3D

export var variance:float = 0.1


onready var defaultPitch = pitch_scale

func randPitch():
	pitch_scale = defaultPitch + rand_range(1.0-variance,1.0+variance)
