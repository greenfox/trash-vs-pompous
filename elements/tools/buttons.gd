tool
extends Sprite

const faceButtons =  {
	A=5,
	B=6,
	X=7,
	Y=8,
	LT=48,
	RT=49,
	LB=51,
	RB=52,
	START=11,
	SELECT=10
}

var faceButtonFrame:Vector2= Vector2(12,6)

var downOffset=12

enum BUTTON {
	A,B,X,Y,LT,LB,RT,RB,
	START,SELECT
#	D_UP,D_DOWN,D_LEFT,D_RIGHT
}

export(BUTTON) var button setget setButton

export var pressed:bool setget setState

func down():
	frame = faceButtons[BUTTON.keys()[button]] + downOffset
func up():
	frame = faceButtons[BUTTON.keys()[button]]


func setButton(value):
	button = value
	setState(pressed)

func setState(value):
	pressed = value
	if pressed:
		down()
	else:
		up()
		

export var prompt:bool = false setget setPrompt

func setPrompt(value:=true):
	if not has_node("AnimationPlayer"):
		yield(self,"ready")
	if value:
		$AnimationPlayer.play("prompt")
	else:
		$AnimationPlayer.stop()
	prompt = value
