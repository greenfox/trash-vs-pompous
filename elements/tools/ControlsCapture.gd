class_name ControlsCapture
extends Node 

enum TYPE{ KEYBOARD }

export(TYPE) var type

var aimVector:Vector2=Vector2.ZERO

export var upForJump:bool= false

func get_controls()->Controls:
	match type:
		TYPE.KEYBOARD:
			return keyboard()
		_:
			return Controls.new()


func keyboard()->Controls:
	var output = Controls.new();
	if Input.is_action_pressed("up"):
		output.direction += Vector2.UP
		if upForJump:
			output.jump = true
	if Input.is_action_pressed("down"):
		output.direction += Vector2.DOWN
	if Input.is_action_pressed("left"):
		output.direction += Vector2.LEFT
	if Input.is_action_pressed("right"):
		output.direction += Vector2.RIGHT
	output.jump = Input.is_action_pressed("jump") or output.jump
	output.justJumped = Input.is_action_just_pressed("jump")
	output.normalize()

	output.fire = Input.is_action_just_pressed("fire")

	aimVector = aimVector.clamped(1)
	if aimVector.length() < 0.3:
		output.aim = Vector2.ZERO
	else:
		output.aim = aimVector

	return output


func _to_string()->String:
	return ""
	

func _unhandled_input(event):
	if event is InputEventMouseMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
#		print(event)
		aimVector += (event.relative * Controls.settings.mouseMult) * Vector2(1,-1)
		
	elif event is InputEventJoypadMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		if event.axis == JOY_AXIS_2:
			aimVector.x = event.axis_value 
		elif event.axis == JOY_AXIS_3:
			aimVector.y = -event.axis_value 
