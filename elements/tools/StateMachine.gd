class_name StateMachine
extends Node

signal transitioned(state_name)

export var initial_state := NodePath()

onready var state: State = get_node(initial_state)
onready var pawn:KinematicBody = get_parent()
onready var old_state = state

var stateTime:float = 0.0

#signal animation(animation)

func _ready() -> void:
	yield(owner, "ready")
	for child in get_children():
		child.state_machine = self
#		(child as State).connect("animation",self,"animation")
	state.enter()

func _unhandled_input(event: InputEvent) -> void:
	state.handle_input(event)

func _process(delta: float) -> void:
	state.update(delta)

func _physics_process(delta: float) -> void:
	stateTime += delta
	state.physics_update(delta)

func transition_to(target_state_name: String, msg: Dictionary = {}) -> void:
	if not has_node(target_state_name):
		return
	state.exit()
	stateTime = 0.0
	state = get_node(target_state_name)
	state.enter(msg)
	emit_signal("transitioned", state.name)

func get_pawn():
	pass
#	get_parent()

#var stateTime setget ,getStateTime
#func getStateTime()->float:
#	return (OS.get_system_time_msecs() - stateTime)/1000.0

