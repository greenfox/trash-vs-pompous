tool
extends Sprite

const faceButtons =  {
	A=5,
	B=6,
	X=7,
	Y=8,
	LT=48,
	RT=49,
	LB=51,
	RB=52,
	START=11,
	SELECT=10,
	D_UP=8,
	D_DOWN=9,
	D_LEFT=10,
	D_RIGHT=11,
}

var faceButtonFrame:Vector2= Vector2(12,6)

var downOffset=12

var faceButton:= {
	frame=Vector2(12,6),
	texture=load("res://assets/buttons/Xbox.png"),
	downOffset=12
	};
var dPad:= {
	frame=Vector2(15,4),
	texture=load("res://assets/buttons/Switch.png"),
	downOffset=15
	};

enum BUTTON {
	A,B,X,Y,LT,LB,RT,RB,
	START,SELECT
	D_UP,D_DOWN,D_LEFT,D_RIGHT
}

export(BUTTON) var button setget setButton

export var pressed:bool setget setState

func down():
	frame = faceButtons[BUTTON.keys()[button]] + downOffset
func up():
	frame = faceButtons[BUTTON.keys()[button]]


func setButton(value):
	button = value
	match value:
		BUTTON.D_UP,BUTTON.D_DOWN,BUTTON.D_LEFT,BUTTON.D_RIGHT:
			setType(dPad)
		_:
			setType(faceButton)
	setState(pressed)

func setState(value):
	pressed = value
	if pressed:
		down()
	else:
		up()
		

export var prompt:bool = false setget setPrompt

func setPrompt(value:=true):
	if not has_node("AnimationPlayer"):
		yield(self,"ready")
	if value:
		$AnimationPlayer.play("prompt")
	else:
		$AnimationPlayer.stop()
	prompt = value

func setType(dict):
	texture = dict.texture
	vframes = dict.frame.y
	hframes = dict.frame.x
	downOffset = dict.downOffset
