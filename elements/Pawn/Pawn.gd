class_name Pawn
extends KinematicBody

export var airTimeForce = 10
#export var gravity:=25
export var runSpeed:=10.0
export var jumpVelocity:=10.0
export var jumpTime:=.5

export var maxSpeed:= 10

export var slideAngle = 45.0 #setget privateSet,privateGet
var position:Vector2 setget setPosition,getPosition

var velocity:=Vector3.ZERO setget setVelocity
var velocityOverride:= Vector3.ZERO 
export var velocityOverrideDampening:= 1.0 
export var maxVelocityOverride:float = 10;
#export var controlTarget:NodePath



func _ready():
	move_lock_z = true;
	set_axis_lock(PhysicsServer.BODY_AXIS_ANGULAR_X,true)
	set_axis_lock(PhysicsServer.BODY_AXIS_ANGULAR_Y,true)
	set_axis_lock(PhysicsServer.BODY_AXIS_ANGULAR_Z,true)




func setPosition(value:Vector2):
	translation = Vector3(value.x , value.y , translation.z)


func getPosition()->Vector2:
	return Vector2(translation.x,translation.y)


var jumpQueued = false
func _physics_process(delta):
	if velocityOverride.length() > maxVelocityOverride:
		velocityOverride = velocityOverride.normalized() * maxVelocityOverride
	translation.z = 0
	controls = $ControlsCapture.get_controls()
	if controls.justJumped:
		jumpQueued = true
		
	velocityOverride = lerp(velocityOverride,Vector3.ZERO, 1- exp(-velocityOverrideDampening * delta))
	var stopLimit = 0.5
	if -stopLimit < velocityOverride.x  and velocityOverride.x < stopLimit and velocityOverride.x != 0:
		velocityOverride.x=0
#public static float Damp(float a, float b, float lambda, float dt)
#{
#    return Mathf.Lerp(a, b, 1 - Mathf.Exp(-lambda * dt))
#}


var controls:Controls = Controls.new()
func getControls()->Controls:
	return controls


var state setget ,getState

func getState()->String:
	return $StateMachine.state.name


func setVelocity(value:Vector3):
	velocity = Vector3(value.x,value.y,0)


func getSlideRad()->float:
	return deg2rad(slideAngle)


func knockBack(value:Vector3):
	velocityOverride.x += value.x
	velocityOverride.y += value.y
	$StateMachine.transition_to("Falling")

