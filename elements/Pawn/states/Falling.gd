extends State

export var coyoteTime:float = 0.2

func physics_update(delta: float) -> void:
	var pawn:Pawn = state_machine.pawn
	
	pawn.velocity += Vector3.DOWN * Globals.gravity * delta
	
	var controls:Controls=pawn.getControls()
	
	pawn.velocity.x += controls.direction.x * pawn.fallingAirtime * delta
	pawn.velocity.x = clamp(pawn.velocity.x,-pawn.maxAirtimeXAxis,pawn.maxAirtimeXAxis)
	
	var collision:= pawn.move_and_collide((pawn.velocity + pawn.velocityOverride)*delta)
	var floored= false
	if collision:
		if collision.normal.angle_to(Vector3.UP) > pawn.getSlideRad():
			pawn.velocity = pawn.velocity.bounce(collision.normal)
			pawn.velocity -= pawn.velocity.project(collision.normal) * 0.9
		else:
			floored = true
	
#	pawn.velocity = pawn.move_and_slide(pawn.velocity + pawn.velocityOverride,Vector3.UP,true,4,pawn.getSlideRad(),false) - pawn.velocityOverride
	
	if pawn.velocity.y > 0:
		pawn.animate("Up")
	else:
		pawn.animate("Fall")
	
	if floored:
		transition_to("Standing")
	elif  state_machine.stateTime< coyoteTime and controls.jump and pawn.jumpQueued and state_machine.old_state.name != "Jumping":
		transition_to("Jumping")
