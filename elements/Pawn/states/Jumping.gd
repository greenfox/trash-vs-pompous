extends State

var remainingTime

func physics_update(delta: float) -> void:
	var pawn:Pawn = state_machine.pawn
	remainingTime -= delta
	var controls:Controls = pawn.getControls()
	
	pawn.velocity.x += controls.direction.x * pawn.jumpingAirtime * delta
	pawn.velocity.x = clamp(pawn.velocity.x,-pawn.maxAirtimeXAxis,pawn.maxAirtimeXAxis)
	pawn.velocity = pawn.move_and_slide(pawn.velocity + pawn.velocityOverride,Vector3.UP,true,4,pawn.getSlideRad(),false) - pawn.velocityOverride
	
	pawn.animate("Up")

	if remainingTime < 0.0 or not controls.jump:
		transition_to("Falling")
	
func enter(_input=null):
	remainingTime = state_machine.pawn.jumpTime
#	(state_machine.pawn as Pawn).position.y = 5
	state_machine.pawn.velocity.y = state_machine.pawn.jumpVelocity
	state_machine.pawn.jumpQueued = false
	
	
