extends State

func physics_update(delta: float) -> void:
	var pawn:Pawn = state_machine.pawn
	var controls:Controls = pawn.getControls()
	
#	var oldVelocity:Vector3= pawn.velocity

	pawn.velocity = Vector3(controls.direction.x*pawn.runSpeed,0,0) + (Vector3.DOWN * Globals.gravity * delta * 5) #this 5 helps make sure snapping happens on steaper slopes
	pawn.velocity.x = clamp(pawn.velocity.x,-pawn.runSpeed,pawn.runSpeed)
	pawn.velocity = pawn.move_and_slide_with_snap(pawn.velocity + pawn.velocityOverride,Vector3.DOWN*2.0,Vector3.UP,true,4,pawn.getSlideRad(),false) - pawn.velocityOverride
	
	if pawn.velocity.x != 0:
		pawn.animate("Walk")
	else:
		pawn.animate("Idle")
	
	if state_machine.pawn.is_on_floor():
		if controls.jump and pawn.jumpQueued:
			transition_to("Jumping")
	else:
		transition_to("Falling")
#	
#	if not pawn.is_on_floor():

